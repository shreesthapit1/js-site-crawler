
const puppeteer = require('puppeteer');
const fs = require('fs');

let depth = 3;
var available_links = [];
let crawled_links = [];
let interval = 2000
let baseUrl = '//example.com';
let crawlUrl = 'https://www.example.com/';

async function crwal(url) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    try {
        let response = await page.goto(url);
        fs.appendFile('response.log', url + ',' + response.status() + '\n', function (err) {

        });
        if (url.includes(baseUrl) && url.indexOf('#') === -1) {
            const hrefs = await page.$$eval('a', as => as.map(a => a.href));

            available_links = available_links.concat(hrefs)
            available_links = [...new Set(available_links)];
        } else {
            console.log('external url ' + url);
        }

    } catch (e) {
        fs.appendFile('response.log', url + ',error' + '\n', function (err) {

        });
    }
    await browser.close();
}

(async () => {

    await crwal(crawlUrl)
    crawled_links.push(crawlUrl)


    for (var i = 0; i < depth; i++) {
        setTimeout(() => {
            available_links.map(async (available_link, index) => {
                if (!checkIfCrawled(available_link)) {
                    await setTimeout(async () => {
                        console.log(available_link)
                        await crwal(available_link)
                        crawled_links.push(available_link)
                    }, interval * index)
                }
            })
        }, interval * i * Object.keys(available_links).length)
    }
})();

function checkIfCrawled(url) {
    return crawled_links.find((el) => el === url)
}
